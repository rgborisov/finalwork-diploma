var numeral = require('numeral');
var bcrypt = require('bcrypt-nodejs');
var dateFormat = require('dateformat');
var { GraphQLSchema } = require('graphql');
var graphqlHTTP = require('express-graphql');
var http = require('http');
var access_api = require('../models/access_api');
var color_codes = require('../models/color_codes');

var schema_all =
`{
    org(orgid: 6) {
        items {
            name
            orgitemid
            properties {
                type
                value
            }
            tags {
                tagid
                valuetype
                tagdescription
            }
        }
    }
}`

exports.loggedIn = function (req, res, next) {
    if (req.session.user) { // req.session.passport._id

        next();

    } else {

        res.redirect('/login');

    }

}

exports.tag_data = function (req, res) {

    access_api.get(schema_all, function (data) {

        var tag_description = "Values";
        data.org.items.forEach(element => {
            element.tags.forEach(tag => {
                if(req.params.tagid == tag.tagid)
                {
                    console.log(tag);
                    tag_description = tag.tagdescription;
                }
            });
        });

        var d = new Date();
        d.setDate(d.getDate() - 14);
        console.log(d.toUTCString());
        var schema =
            `{
                after(tagid: ${req.params.tagid}, count: 1000, date: "${d.toUTCString()}") {
                    date
                    value
                }
            }`;
            
        access_api.get(schema, function (tag_data) {
            console.log(req.params.tagid, tag_data.length);
            // console.log(tag_data);
            if (tag_data && tag_data.after) {
                tag_data = tag_data.after;
                send_sensor_data = [['Last two weeks', tag_description], [], []];
                if (tag_description in color_codes) {
                    send_sensor_data[0].push(color_codes[tag_description]);
                }
                // send_sensor_data = [];
                tag_data.forEach(element => {
                    // send_sensor_data.push([element.date, element.value]);
                    var d = Date.parse(element.date);
                    send_sensor_data[1].push(d);
                    send_sensor_data[2].push(element.value);
                });
                // console.log(send_sensor_data);
                res.json(send_sensor_data);
            }
            else {
                res.status(500);
            }
        });
    });
}

exports.last_tag_data = function (req, res) {

        var schema =
            `{
                last(tagid: ${req.params.tagid}) {
                    date
                    value
                }
            }`;
            
        access_api.get(schema, function (tag_data) {
            console.log(req.params.tagid, tag_data);
            console.log(tag_data);
            var color_code = 0;
            if (req.params.tagdescription in color_codes) {
                
                // red code
                if (tag_data.last[0].value > color_codes[req.params.tagdescription][1])
                    color_code = 2;
                // orange code
                else if (tag_data.last[0].value > color_codes[req.params.tagdescription][0])
                    color_code = 1;
                // green code
                else color_code = 0;
            }
            if (tag_data && tag_data.last && tag_data.last[0]) {
                tag_data = tag_data.last[0].value;
                res.json({value: tag_data, i: req.params.i, tag_i: req.params.tag_i, color_code: color_code});
            }
            else {
                res.status(500);
            }
        });
}

exports.sensor_data = function (req, res) {

    access_api.get(schema_all, function (data) {
        console.log(data.org.items);
        console.log(req.params.typeid);
        sensor_data = data.org.items.find(x => x.orgitemid == req.params.typeid);
        console.log(sensor_data);
        
        tagids = [];

        if(sensor_data) {
            sensor_data.tags.forEach(tag => {
                tagids.push(tag.tagid);
            });
        }
        console.log(tagids);

        res.render('sensor_data.ejs', {
            title: "Sensor",
            error: req.flash("error"),
            success: req.flash("success"),
            session: req.session,
            sensor_data: sensor_data,
            graphqldata: JSON.stringify(data.org.items),
            tagids: tagids

        });
    
    });


}
