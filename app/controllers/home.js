var numeral = require('numeral');
var bcrypt = require('bcrypt-nodejs');
var dateFormat = require('dateformat');
var { GraphQLSchema } = require('graphql');
var graphqlHTTP = require('express-graphql');
var http = require('http');
var access_api = require('../models/access_api');

exports.loggedIn = function (req, res, next) {
	if (req.session.user) { // req.session.passport._id

		next();

	} else {

		res.redirect('/login');

	}

}

exports.home = function (req, res) {

	var schema = 
	`{
		org(orgid: 6) {
			items {
				name
				orgitemid
				properties {
					type
					value
				}
				tags {
					tagid
					valuetype
					tagdescription
				}
			}
		}
	}`

	access_api.get(schema, function (resp) {
		console.log(JSON.stringify(resp.org.items));
		res.render('index.ejs', {
			title: "Home",
			error: req.flash("error"),
			success: req.flash("success"),
			session: req.session,
			graphqldata: JSON.stringify(resp.org.items)

		});
	});


}


exports.signup = function (req, res) {

	if (req.session.user) {

		res.redirect('/home');

	} else {

		res.render('signup', {
			error: req.flash("error"),
			success: req.flash("success"),
			session: req.session
		});
	}

}


exports.login = function (req, res) {


	if (req.session.user) {

		res.redirect('/home');

	} else {

		res.render('login', {
			error: req.flash("error"),
			success: req.flash("success"),
			session: req.session
		});

	}

}
