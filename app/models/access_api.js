var constants = require('../../config/constants');
var request = require('request');
var XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

var access_token = "";
function refresh_tok(callback) {
	request.post({
		url: "http://aspires.icb.bg/identityserver/connect/token",
		headers: {
			"Content-Type": "application/x-www-form-urlencoded",
		},
		form: {
			client_id: constants.graphql_clientID,
			client_secret: constants.graphql_secret,
			grant_type: "client_credentials"
		}
	}, function (error, response, body) {
		access_token = JSON.parse(body).access_token
		console.log(access_token);
		callback();
	});
}
refresh_tok(function name(params) {});

exports.get = function (schema, callback) {
	// var schema = "{org(orgid: 6) {items{name}}}";
	console.log(access_token);

	var url = "http://aspires.icb.bg/query/api/graphql?query=" + schema;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", url);
	xhr.setRequestHeader("Authorization", "Bearer " + access_token);
	xhr.send();
	xhr.onload = function () {
		try {
			response = JSON.parse(xhr.responseText).data;
		} catch (error) {
			console.log(xhr.responseText);
			refresh_tok(function name(params) {
				exports.get(schema, callback);
			});
		}
		callback(response);
	}
};