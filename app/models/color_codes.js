module.exports =  {
    "Lora Signal to Noise Ratio": [10, 10, 20],
    "Dust": [20, 100, 200],
    "RH": [40, 60, 100],
    "CO2": [1500, 2200, 3000],
    "Temperature": [25, 40, 100],
    "Wind Speed": [50, 70, 100],
    "Max Preset Temperature": [60, 150, 200],
    "CO": [3, 8, 10]
}
