var home = require('../app/controllers/home');
var sensors = require('../app/controllers/sensors');

//you can include all your controllers

module.exports = function (app, passport) {

    app.get('/login', home.login);
    app.get('/signup', home.signup);
    app.get('/logout', function(req, res){
        req.session.destroy();
        req.logout();
        res.redirect('/');
    });

    app.get('/', home.loggedIn, home.home);//home
    app.get('/home', home.loggedIn, home.home);//home

    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/home', // redirect to the secure profile section
        failureRedirect: '/signup', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));
    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/home', // redirect to the secure profile section
        failureRedirect: '/login', // redirect back to the signup page if there is an error
        failureFlash: true // allow flash messages
    }));


    app.get('/sensor_data/:typeid', home.loggedIn, sensors.sensor_data);
    app.get('/sensor_data/tag/:tagid', home.loggedIn, sensors.tag_data);
    app.get('/sensor_data/tag_last/:tagid/:i/:tag_i/:tagdescription', home.loggedIn, sensors.last_tag_data);
    app.get('/sensor_data/tag_last/:tagid/:i/:tag_i/', home.loggedIn, sensors.last_tag_data);

}
